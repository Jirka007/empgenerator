
// Example dtoIn
const dtoIn = {
  count: 2,
  age: {min: 20, max: 35}
};

const firstNameMale = ["Jaroslav", "Vladimír", "Petr", "Tomáš", "Lukáš", "Martin", "Michal", "Jakub", "Pavel", "Josef", "František", "Jiří", "David", "Miroslav", "Václav", "Karel", "Robert", "Zdeněk", "Radek", "Vladimír", "Milan", "Ondřej", "Aleš", "Patrik", "Vojtěch", "Štěpán", "Filip", "Marek", "Adam", "Richard", "Daniel", "Petr", "Jiří", "Josef", "Jan", "Martin", "Lukáš", "Michal", "Jakub", "Tomáš", "Miroslav", "David", "Karel", "Václav", "Robert", "Pavel", "Ondřej", "Jan",];
const lastNameMale = ["Němec", "Marek", "Pospíšil", "Pražák", "Svoboda", "Kocábek", "Hynek", "Komárek", "Horák", "Novák", "Svoboda", "Novotný", "Hájek", "Král", "Růžička", "Beneš", "Fiala", "Sedláček", "Kříž", "Jelínek", "Kolář", "Vaněk", "Šimek", "Bartoš", "Konečný", "Holub", "Matějka", "Urban", "Blažek", "Hlaváček", "Novák", "Svoboda", "Novotný", "Dvořák", "Černý", "Procházka", "Kučera", "Veselý", "Horák", "Němec", "Marek", "Pospíšil", "Hájek", "Král", "Růžička", "Beneš", "Fiala", "Sedláček", "Kříž", "Jelínek"];
const firstNameFemale = ["Anežka", "Petra", "Martina", "Hana", "Alena", "Jana", "Jitka", "Eva", "Lucie", "Anna", "Lenka", "Karolína", "Ivana", "Marta", "Zuzana", "Veronika", "Tereza", "Michaela", "Barbora", "Markéta", "Simona", "Nela", "Kateřina", "Šárka", "Jitka", "Nikola", "Monika", "Kristýna", "Lada", "Kamila", "Věra", "Jarmila", "Libuše", "Dana", "Iveta", "Renata", "Jindra", "Linda", "Radka", "Aneta", "Romana", "Lenka", "Karolína", "Alena", "Marta", "Zuzana", "Veronika", "Tereza", "Michaela"];
const lastNameFemale = ["Drahotová", "Pražáková", "Tichá", "Karousková", "Zemanová", "Suchá", "Dlouhá", "Veselá", "Horáková", "Němcová", "Marková", "Pospíšilová", "Hájková", "Králová", "Růžičková", "Benešová", "Fialová", "Sedláčková", "Křížová", "Jelínková", "Kolářová", "Vaněková", "Šimková", "Bartošová", "Konečná", "Holubová", "Matějková", "Urbanová", "Blažková", "Hlaváčková", "Nováková", "Svobodová", "Novotná", "Dvořáková", "Černá", "Procházková", "Kučerová", "Veselá", "Horáková", "Němcová", "Marková", "Pospíšilová", "Hájková", "Králová", "Růžičková", "Benešová", "Fialová", "Sedláčková", "Křížová", "Jelínková"];
function main(dtoIn) {

  function getRandomBirthdate(minAge, maxAge) {
    const currentYear = new Date().getFullYear();
    const year = currentYear - Math.floor(Math.random() * (maxAge - minAge + 1)) - minAge;
    const month = Math.floor(Math.random() * 12) + 1;
    const day = Math.floor(Math.random() * 28) + 1;
    return new Date(year, month - 1, day).toISOString();
  }

  function getRandomElement(array) {
    return array[Math.floor(Math.random() * array.length)];
  }

  function getRandomWorkload() {
    const workloads = [10, 20, 30, 40];
    return getRandomElement(workloads);
  }

  const dtoOut = [];
  let gender = '';

  for (let i = 0; i < dtoIn.count; i++) {
    if (Math.random() > 0.5) {
      gender = 'male';
    } else {
      gender = 'female';
    }
    if (gender === 'male') {
      firstName = getRandomElement(firstNameMale);
      lastName = getRandomElement(lastNameMale);
    } else{
      firstName = getRandomElement(firstNameFemale);
      lastName = getRandomElement(lastNameFemale);
    }
    const birthdate = getRandomBirthdate(dtoIn.age.min, dtoIn.age.max);
    const workload = getRandomWorkload();

    dtoOut.push({gender, birthdate, firstName, lastName, workload});
  }


  return dtoOut;
}


console.log(main(dtoIn));
